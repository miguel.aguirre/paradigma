# paradigma

PHP Software Engineer Technical Evaluation

He realizado este proyecto como prueba t�cnica de mi candidatura con Paradigma

Existen dos partes diferenciadas dentro de la aplicaci�n, la p�gina home y la p�gina feed (que pretende simular un sitio externo desde el que cargar los datos)

La p�gina home contiene una lista de encuentros, el lugar donde ocurrir� y el resultado del mismo en caso de haber sido ya jugado
Si los partidos ya realizados (de fecha anterior a la actual) tienen informacion, se podr� cargar la tabla para visualizarlos
Si los partidos han sido realizados pero no se dispone de datos acerca del mismo, aparecer� el resultado pero no se habilitar� el bot�n que muestra los datos
La barra superior es desplegable y tiene un bot�n para refrescar la tabla

La p�gina feed contiene un selector con el que indicar el rival del encuentro sobre el que se quiere actuar
Al seleccionarlo se obtiene la tabla con los datos ya insertados y se carga un formulario con los campos necesarios para realizar una inserci�n
Cuando se insertan datos, los campos de selecci�n vuelven al estado inicial para impedir dobles inserciones por error. Si la inserci�n es 'inicio' y ya existe, no se inserta
Cuando el evento insertado es 'gol', se verifica el equipo que anota y se a�ade a su resultado, actualiz�ndose as� la tabla en home en tiempo real


Posibles mejoras detectadas:
Al hacer una inserci�n en feed, recargar la tabla para mostrar el nuevo campo, para lo cual hay que externalizar la funci�n para llamarla en ambos casos. Por ahora es una es una funci�n an�nima que solo se ejecuta al cambiar el rival
Recargas peri�dicas de la tabla de home que muestra el resultado, ya que se podr�a ver a tiempo real el resultado parcial de un encuentro que se est� disputando
El bot�n refresh que tiene la barra de navegaci�n, deber�a recargar solo la tabla
Se deber�a controlar la inserci�n de final de partido para impedir inserci�nes cuya hora sea superior
Se deber�a controlar que la primera inserci�n de un partido fuese siempre 'inicio'
Se deber�a controlar si un partido ha finalizado o no para mostrar el resultado como definitivo y, entre tanto, no mostrar 'ganado', 'empatado' o 'perdido'


Las pruebas realizadas han sido a lo largo del desarrollo y no todo ha sido verificado por la escasez de tiempo, ser�a necesario dedicarle un tiempo mucho mayor junto a un an�lisis m�s extenso para prevenir errores
He intentado implementar esta soluci�n en una instancia EC2 para la visualizaci�n de la misma en un entorno estable, pero no me ha sido posible verificar su uso en un tiempo prudencial, 
por esa raz�n he incluido una carpeta en la ra�z del proyecto, llamada 'DB', con el c�digo SQL necesario para dejar lista la base de datos para test en un entorno local
Para que todo funcione correctamente, es necesario que el host, el puerto, el usuario y la contrase�a de la base de datos a utilizar coincidan con los par�metros hom�nimos del fichero '[carpeta_raiz_del_proyecto]/app/config/parameters.yml'

Gracias por su tiempo

Atentamente,
Miguel Aguirre Varela