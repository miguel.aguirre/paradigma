-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.23 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para paradigma
DROP DATABASE IF EXISTS `paradigma`;
CREATE DATABASE IF NOT EXISTS `paradigma` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `paradigma`;

-- Volcando estructura para tabla paradigma.fixtures
DROP TABLE IF EXISTS `fixtures`;
CREATE TABLE IF NOT EXISTS `fixtures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rival_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fixture_date` datetime NOT NULL,
  `self_score` smallint(6) NOT NULL,
  `rival_score` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla paradigma.fixtures: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `fixtures` DISABLE KEYS */;
INSERT INTO `fixtures` (`id`, `rival_name`, `location`, `fixture_date`, `self_score`, `rival_score`) VALUES
	(1, 'Madrid', 'Bernabeu', '2019-03-05 14:22:00', 0, 0),
	(2, 'Barcelona', 'Nou Camp', '2019-03-01 13:00:00', 0, 0),
	(3, 'Atlético', 'García de la Mata', '2019-03-03 14:00:00', 0, 0),
	(4, 'Coruña', 'Riazor', '2019-03-11 11:40:00', 0, 0),
	(5, 'Vigo', 'García de la Mata', '2019-03-15 14:00:00', 0, 0),
	(6, 'Sevilla', 'Ramón Sánchez-Pizjuán', '2019-03-17 14:00:00', 0, 0);
/*!40000 ALTER TABLE `fixtures` ENABLE KEYS */;

-- Volcando estructura para tabla paradigma.match_log
DROP TABLE IF EXISTS `match_log`;
CREATE TABLE IF NOT EXISTS `match_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `eventTime` time NOT NULL,
  `playerId` int(11) NOT NULL,
  `fixtureId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla paradigma.match_log: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `match_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `match_log` ENABLE KEYS */;

-- Volcando estructura para tabla paradigma.player_list
DROP TABLE IF EXISTS `player_list`;
CREATE TABLE IF NOT EXISTS `player_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `teamId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla paradigma.player_list: ~77 rows (aproximadamente)
/*!40000 ALTER TABLE `player_list` DISABLE KEYS */;
INSERT INTO `player_list` (`id`, `name`, `teamId`) VALUES
	(1, 'madrid1', 2),
	(2, 'madrid2', 2),
	(3, 'madrid3', 2),
	(4, 'madrid4', 2),
	(5, 'madrid5', 2),
	(6, 'madrid6', 2),
	(7, 'madrid7', 2),
	(8, 'madrid8', 2),
	(9, 'madrid9', 2),
	(10, 'madrid10', 2),
	(11, 'madrid11', 2),
	(12, 'barcelona1', 1),
	(13, 'barcelona2', 1),
	(14, 'barcelona3', 1),
	(15, 'barcelona4', 1),
	(16, 'barcelona5', 1),
	(17, 'barcelona6', 1),
	(18, 'barcelona7', 1),
	(19, 'barcelona8', 1),
	(20, 'barcelona9', 1),
	(21, 'barcelona10', 1),
	(22, 'barcelona11', 1),
	(23, 'coruña1', 4),
	(24, 'coruña2', 4),
	(25, 'coruña3', 4),
	(26, 'coruña4', 4),
	(27, 'coruña5', 4),
	(28, 'coruña6', 4),
	(29, 'coruña7', 4),
	(30, 'coruña8', 4),
	(31, 'coruña9', 4),
	(32, 'coruña10', 4),
	(33, 'coruña11', 4),
	(34, 'atletico1', 3),
	(35, 'atletico2', 3),
	(36, 'atletico3', 3),
	(37, 'atletico4', 3),
	(38, 'atletico5', 3),
	(39, 'atletico6', 3),
	(40, 'atletico7', 3),
	(41, 'atletico8', 3),
	(42, 'atletico9', 3),
	(43, 'atletico10', 3),
	(44, 'atletico11', 3),
	(45, 'vigo1', 5),
	(46, 'vigo2', 5),
	(47, 'vigo3', 5),
	(48, 'vigo4', 5),
	(49, 'vigo5', 5),
	(50, 'vigo6', 5),
	(51, 'vigo7', 5),
	(52, 'vigo8', 5),
	(53, 'vigo9', 5),
	(54, 'vigo10', 5),
	(55, 'vigo11', 5),
	(56, 'sevilla1', 6),
	(57, 'sevilla2', 6),
	(58, 'sevilla3', 6),
	(59, 'sevilla4', 6),
	(60, 'sevilla5', 6),
	(61, 'sevilla6', 6),
	(62, 'sevilla7', 6),
	(63, 'sevilla8', 6),
	(64, 'sevilla9', 6),
	(65, 'sevilla10', 6),
	(66, 'sevilla11', 6),
	(67, 'adarve1', 7),
	(68, 'adarve2', 7),
	(69, 'adarve3', 7),
	(70, 'adarve4', 7),
	(71, 'adarve5', 7),
	(72, 'adarve6', 7),
	(73, 'adarve7', 7),
	(74, 'adarve8', 7),
	(75, 'adarve9', 7),
	(76, 'adarve10', 7),
	(77, 'adarve11', 7);
/*!40000 ALTER TABLE `player_list` ENABLE KEYS */;

-- Volcando estructura para tabla paradigma.teams
DROP TABLE IF EXISTS `teams`;
CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TeamName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `StadiumName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_96C22258109F2030` (`TeamName`),
  UNIQUE KEY `UNIQ_96C2225891F63C11` (`StadiumName`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla paradigma.teams: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` (`id`, `TeamName`, `StadiumName`) VALUES
	(1, 'Barcelona', 'Nou Camp'),
	(2, 'Madrid', 'Bernabeu'),
	(3, 'Atlético', 'Metropolitano'),
	(4, 'Coruña', 'Riazor'),
	(5, 'Vigo', 'Balaídos'),
	(6, 'Sevilla', 'Ramón Sánchez-Pizjuán'),
	(7, 'Adarve', 'García de la Mata');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
