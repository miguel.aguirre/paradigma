<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Teams
 *
 * @ORM\Table(name="teams")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamsRepository")
 */
class Teams
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="TeamName", type="string", length=50, unique=true)
     */
    private $teamName;

    /**
     * @var string
     *
     * @ORM\Column(name="StadiumName", type="string", length=50, unique=true)
     */
    private $stadiumName;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set teamName
     *
     * @param string $teamName
     *
     * @return Teams
     */
    public function setTeamName($teamName)
    {
        $this->teamName = $teamName;

        return $this;
    }

    /**
     * Get teamName
     *
     * @return string
     */
    public function getTeamName()
    {
        return $this->teamName;
    }

    /**
     * Set stadiumName
     *
     * @param string $stadiumName
     *
     * @return Teams
     */
    public function setStadiumName($stadiumName)
    {
        $this->stadiumName = $stadiumName;

        return $this;
    }

    /**
     * Get stadiumName
     *
     * @return string
     */
    public function getStadiumName()
    {
        return $this->stadiumName;
    }
}

