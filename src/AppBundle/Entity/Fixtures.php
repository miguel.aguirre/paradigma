<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fixtures
 *
 * @ORM\Table(name="fixtures")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FixturesRepository")
 */
class Fixtures
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rival_name", type="string", length=50)
     */
    private $rivalName;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=50)
     */
    private $location;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fixture_date", type="datetime")
     */
    private $fixtureDate;

    /**
     * @var int
     *
     * @ORM\Column(name="self_score", type="smallint")
     */
    private $selfScore;

    /**
     * @var int
     *
     * @ORM\Column(name="rival_score", type="smallint")
     */
    private $rivalScore;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rivalName
     *
     * @param string $rivalName
     *
     * @return Fixtures
     */
    public function setRivalName($rivalName)
    {
        $this->rivalName = $rivalName;

        return $this;
    }

    /**
     * Get rivalName
     *
     * @return string
     */
    public function getRivalName()
    {
        return $this->rivalName;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Fixtures
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set fixtureDate
     *
     * @param \DateTime $fixtureDate
     *
     * @return Fixtures
     */
    public function setFixtureDate($fixtureDate)
    {
        $this->fixtureDate = $fixtureDate;

        return $this;
    }

    /**
     * Get fixtureDate
     *
     * @return \DateTime
     */
    public function getFixtureDate()
    {
        return $this->fixtureDate;
    }

    /**
     * Set selfScore
     *
     * @param integer $selfScore
     *
     * @return Fixtures
     */
    public function setSelfScore($selfScore)
    {
        $this->selfScore = $selfScore;

        return $this;
    }

    /**
     * Get selfScore
     *
     * @return int
     */
    public function getSelfScore()
    {
        return $this->selfScore;
    }

    /**
     * Set rivalScore
     *
     * @param integer $rivalScore
     *
     * @return Fixtures
     */
    public function setRivalScore($rivalScore)
    {
        $this->rivalScore = $rivalScore;

        return $this;
    }

    /**
     * Get rivalScore
     *
     * @return int
     */
    public function getRivalScore()
    {
        return $this->rivalScore;
    }
}

