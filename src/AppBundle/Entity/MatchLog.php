<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MatchLog
 *
 * @ORM\Table(name="match_log")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MatchLogRepository")
 */
class MatchLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="eventName", type="string", length=50)
     */
    private $eventName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eventTime", type="time")
     */
    private $eventTime;

    /**
     * @var int
     *
     * @ORM\Column(name="playerId", type="integer")
     */
    private $playerId;

    /**
     * @var int
     *
     * @ORM\Column(name="fixtureId", type="integer")
     */
    private $fixtureId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventName
     *
     * @param string $eventName
     *
     * @return MatchLog
     */
    public function setEventName($eventName)
    {
        $this->eventName = $eventName;

        return $this;
    }

    /**
     * Get eventName
     *
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }

    /**
     * Set eventTime
     *
     * @param \DateTime $eventTime
     *
     * @return MatchLog
     */
    public function setEventTime($eventTime)
    {
        $this->eventTime = $eventTime;

        return $this;
    }

    /**
     * Get eventTime
     *
     * @return \DateTime
     */
    public function getEventTime()
    {
        return $this->eventTime;
    }

    /**
     * Set playerId
     *
     * @param integer $playerId
     *
     * @return MatchLog
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;

        return $this;
    }

    /**
     * Get playerId
     *
     * @return int
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * Set fixtureId
     *
     * @param integer $fixtureId
     *
     * @return MatchLog
     */
    public function setFixtureId($fixtureId)
    {
        $this->fixtureId = $fixtureId;

        return $this;
    }

    /**
     * Get fixtureId
     *
     * @return integer
     */
    public function getFixtureId()
    {
        return $this->fixtureId;
    }
}
