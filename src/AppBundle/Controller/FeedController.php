<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Controller\HomeController;
use DateTime;

class FeedController extends Controller
{
    /**
     * Feed controller function which fetch the view
     * which send data to server
     * 
     * @Route("/feed", name="feed")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $matchesList = HomeController::matchesList($em); // TODO this call should be changed when UtilsController were ready
        $selfPlayers = $em->getRepository('AppBundle:PlayerList')->findBy(
                array(
                    'teamId' => 7 // id del club al que pertenece este sitio web
                )
            );

        /* Render feed page with matches list */
        return $this->render('default/feed.html.twig',array('matches'=>$matchesList,'selfPlayers'=>$selfPlayers));
    }
    
}
