<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;

class HomeController extends Controller
{
    /**
     * Main controller function which load all 
     * fixtures from database and send it to view
     * 
     * @Route("/home", name="home")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /* Get all from Fixtures table ordered by date */
        $fixtureList = self::matchesList($em);

        $matchData = array();
        foreach ($fixtureList as $item){
            
            $result = self::matchData($item->getId(),$em);
            if($result){
                $matchData[$item->getId()]['matchData'] = 'data';  
            }else{
                $matchData[$item->getId()]['matchData'] = 'noData';
            }
            $matchData[$item->getId()]['fixtureList'] = $item;
        }
        
        /* Render home page sending $fixtureList as parameter */
        return $this->render('default/home.html.twig', 
                array(
                    'fixtureList' => ($fixtureList?:null), // Send explicit null if resultset is empty
                    'matchData' => $matchData 
                )
            );
    }
    
    public function matchesList($em) {
        return $em->getRepository('AppBundle:Fixtures')->findBy(
                array(),
                array('fixtureDate' => 'ASC')
            );        
    }
    public function matchData($id, $em) {

        return $em->getRepository('AppBundle:MatchLog')->findBy(
                array(
                    'fixtureId' => $id
                ),
                array(
                    'eventTime' => 'ASC'
                )
            );
    }

}
