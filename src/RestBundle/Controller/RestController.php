<?php

namespace RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\MatchLog;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use DateTime;

class RestController extends Controller
{
    /**
     * @Route("/api/matchData")
     */
    public function matchDataAction(Request $request)
    {
        $rivalName = $_POST['rivalName'];
        $em = $this->getDoctrine()->getManager();
        $fixture = $em->getRepository('AppBundle:Fixtures')->findOneBy(
                array(
                    'rivalName' => $rivalName
                )
            );
        $matchData = $em->getRepository('AppBundle:MatchLog')->findBy(
                array(
                    'fixtureId' => $fixture->getId()
                ),
                array(
                    'eventTime' => 'ASC'
                )
            );
        $allData = array();
        $count=0;
        foreach ($matchData as $item){
            $player = $em->getRepository('AppBundle:PlayerList')->find($item->getPlayerId());
            $allData[$count]['time'] = $item->getEventTime();
            $allData[$count]['name'] = $item->getEventName();
            $allData[$count]['player'] = $player->getName();
            $count++;
        }
        $response = new JsonResponse(array(
            'allData' => $allData?:null));
        return $response;
    }
    
    /**
     * @Route("/api/playerList")
     */
    public function playerListAction(Request $request)
    {
        $rivalName = $_POST['rivalName'];
        $em = $this->getDoctrine()->getManager();
        $id = $em->getRepository('AppBundle:Teams')->findOneBy(
                array(
                    'teamName' => $rivalName
                )
            );

        $rivalPlayers = $em->getRepository('AppBundle:PlayerList')->findBy(
                array(
                    'teamId' => $id->getId()
                )
            );
        $rivalData = array();
        $count=0;
        foreach ($rivalPlayers as $item){
            $rivalData[$count]['team'] = $id->getTeamName();
            $rivalData[$count]['name'] = $item->getName();
            $count++;
        }
        $response = new JsonResponse(array(
            'rivalData' => $rivalData?:null));
        return $response;
    }
    
    /**
     * @Route("/api/saveEvent")
     */
    public function saveEventAction(Request $request)
    {

        $rival = $_POST['rival'];
        $event = $_POST['event'];
        $selfPlayer = $_POST['selfPlayer'];
        $rivalPlayer = $_POST['rivalPlayer'];
        $timeEvent = $_POST['time'];
        
        $em = $this->getDoctrine()->getManager();
        
        $player = ($selfPlayer!= 'none'?$selfPlayer:$rivalPlayer);
        $playerId = $em->getRepository('AppBundle:PlayerList')->findOneBy(
                array(
                    'name' => $player
                )
            );
        $fixtureId = $em->getRepository('AppBundle:Fixtures')->findOneBy(
                array(
                    'rivalName' => $rival
                )
            );
        if($event=='inicio'){
            $started = $em->getRepository('AppBundle:MatchLog')->findOneBy(
                array(
                    'fixtureId' => $fixtureId->getId(),
                    'eventName' => 'inicio'
                )
            );
            if($started){
                $response = new JsonResponse(array(
                    'status' => 'ok'));
                return $response;
            }
        }
        $matchEvent = new MatchLog();
        $matchEvent->setEventName($event);

        $date = new DateTime('2016/01/01 '.$timeEvent);
        $dateFormat = $date->format('Y/m/d H:i:s');
        $dateAux = new DateTime($dateFormat);
        $time = new DateTime($dateAux->format('H:i:s'));
        


        $matchEvent->setEventTime($time);
        

        $matchEvent->setFixtureId($fixtureId->getId());
        $matchEvent->setPlayerId($playerId->getId());
        if($event=='gol'){
            if($playerId->getTeamId() == 7){
                $newScore = $fixtureId->getSelfScore() + 1;
                $fixtureId->setSelfScore($newScore);
            }else{
                $newScore = $fixtureId->getRivalScore() + 1;
                $fixtureId->setRivalScore($newScore);
            }
            $em->persist($fixtureId);
            
        }
        $em->persist($matchEvent);
        $em->flush();
        
        $response = new JsonResponse(array(
            'status' => 'ok'));
        return $response;
    }
    
    /**
     * @Route("/api/viewData")
     */
    public function viewDataAction(Request $request)
    {
        $fixtureId = $_POST['fixtureId'];
        $em = $this->getDoctrine()->getManager();
        
        $matchData = $em->getRepository('AppBundle:MatchLog')->findBy(
                array(
                    'fixtureId' => $fixtureId
                ),
                array(
                    'eventTime' => 'ASC'
                )
            );
        $allData = array();
        $count=0;
        foreach ($matchData as $item){
            $player = $em->getRepository('AppBundle:PlayerList')->find($item->getPlayerId());
            $allData[$count]['time'] = $item->getEventTime();
            $allData[$count]['name'] = $item->getEventName();
            $allData[$count]['player'] = $player->getName();
            $count++;
        }
        $response = new JsonResponse(array(
            'allData' => $allData?:null));
        return $response;
    }
    
}
